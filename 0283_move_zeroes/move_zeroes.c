void moveZeroes(int* nums, int numsSize) {
    int none_zero_index[numsSize];
    int num_none_zero = 0;
    int i = 0;
    
    for(i = 0; i < numsSize; i++)
    {
        if(nums[i] != 0)
        {
            none_zero_index[num_none_zero] = i;
            num_none_zero++;
        }
    }
    
    for(i=0; i<num_none_zero; i++)
    {
        nums[i] = nums[none_zero_index[i]];
    }
    
    memset(&nums[num_none_zero], 0, sizeof(int)*(numsSize-num_none_zero));
}
