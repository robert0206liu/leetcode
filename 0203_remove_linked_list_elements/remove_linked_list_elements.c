/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* removeElements(struct ListNode* head, int val) {
    struct ListNode *curr_node = NULL;
    struct ListNode *prev_node = NULL;
    
    if (head != NULL)
    {
        curr_node = head;
        prev_node = NULL;
        
        while(curr_node)
        {
            if(curr_node->val == val)
            {
                if(curr_node->next != NULL)
                {
                    curr_node->val = curr_node->next->val;
                    curr_node->next = curr_node->next->next;
                }
                else
                {
                    if(prev_node != NULL)
                    {
                        prev_node->next = NULL;
                        break;
                    }
                    else
                    {
                        return NULL;
                    }
                }
            }
            
            if(curr_node->val != val)
            {
                prev_node = curr_node;
                curr_node = curr_node->next;
            }
        }
    }
    return head;
}
