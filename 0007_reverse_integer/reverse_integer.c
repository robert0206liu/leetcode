int reverse(int x) {
    long long int new_int = 0;

    while(x)
    {
        new_int = new_int*10 + x%10;
        x /= 10;
    }
    
    if((new_int > 0x7FFFFFFF) || (new_int < (-0x7FFFFFFF - 1)))
    {
        return 0;
    }
    
    return new_int;
}
