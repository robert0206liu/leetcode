void rotate(int** matrix, int matrixRowSize, int matrixColSize) {
    int left_up[2] = {0, 0};
    int right_up[2] = {0, matrixRowSize-1};
    int right_down[2] = {matrixColSize-1, matrixRowSize-1};
    int left_down[2] = {matrixColSize-1, 0};
    
    int i = 0;
    int temp = 0;
    int matrix_level = matrixRowSize/2;
    int length = matrixRowSize-1;
    
    
    while(matrix_level>0)
    {
        for (i = 0; i < length; i++)
        {
            temp = matrix[ left_up[0] ][ left_up[1]+i ];
            matrix[ left_up[0] ][ left_up[1]+i ] = matrix[ left_down[0]-i ][ left_down[1] ];
            matrix[ left_down[0]-i ][ left_down[1] ] = matrix[ right_down[0] ][ right_down[1]-i ];
            matrix[ right_down[0] ][ right_down[1]-i ] = matrix[ right_up[0]+i ][ right_up[1] ];
            matrix[ right_up[0]+i ][ right_up[1] ] = temp;
        }
        left_up[0]++;
        left_up[1]++;
        right_up[0]++;
        right_up[1]--;
        right_down[0]--;
        right_down[1]--;
        left_down[0]--;
        left_down[1]++;
        length -= 2;
        matrix_level--;
    }
}
