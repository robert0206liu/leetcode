bool isHappy(int n) {
    
    int num = 0;
    int *flag = malloc(sizeof(int) * 243);
    memset(flag, 0, sizeof(int) * 243);
    
    // 999 -> 9^2 + 9^2 + 9^2 = 243
    while(n > 243)
    {
        while(n)
        {
            num += pow(n % 10, 2);
            n /= 10;
        }
        n = num;
        num = 0;
    }
    
    while(n != 1)
    {
        while(n)
        {
            num += pow(n % 10, 2);
            n /= 10;
        }
        
        if(flag[num] == 0)
        {
            flag[num] = 1;
        }
        else
        {
            return false;
        }
        n = num;
        num = 0;
    }
    return true;
}
