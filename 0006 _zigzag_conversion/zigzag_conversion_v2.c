char* convert(char* s, int numRows) {
    int s_len = strlen(s);
    int new_s_len = 0;
    char *new_s = NULL;
    int i = 0;
    int j = 0;
    int row = 0;
    int middle_count = 1;
    
    int pattern_num = ceil((float)s_len / (2 * numRows - 2));
    int pattern_size = 2 * numRows - 2;
    
    if(s_len == 0)
    {
        return s;
    }
    
    if(numRows == 1)
    {
        return s;
    }
    
    new_s = malloc(sizeof(char) * (s_len + 1));
    memset(new_s, 0, sizeof(char) * (s_len + 1));
    
    for (i = 0; i < pattern_num; i++, new_s_len++)
    {
        if ((i*pattern_size) < s_len)
        {
            new_s[new_s_len] = s[i*pattern_size];
        }
    }
    
    row++;
    
    if (numRows >2)
    {
        for (i = 0; i < numRows - 2; i++)
        {
            for (j = 0; j < pattern_num; j++)
            {
                if ((row + j*pattern_size) < s_len)
                {
                    new_s[new_s_len] = s[row + j*pattern_size];
                    new_s_len++;
                }
                
                if (((j+1)*pattern_size - middle_count) < s_len)
                {
                    new_s[new_s_len] = s[(j+1)*pattern_size - middle_count];
                    new_s_len++;
                }
            }
            middle_count++;
            row++;
        }
    }
    
    
    for (i = 0; i < pattern_num; i++, new_s_len++)
    {
        if ((row + i*pattern_size) < s_len)
        {
            new_s[new_s_len] = s[row + i*pattern_size];
        }
    }
    
    new_s[s_len] = '\0';
    
    return new_s;
}
