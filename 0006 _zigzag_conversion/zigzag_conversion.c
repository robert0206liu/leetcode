char* convert(char* s, int numRows) {
    int s_len = strlen(s);
    int row_size = 0;
    int col_size = 0;
    int offset = 0;
    char *array = NULL;
    char *new_s = NULL;
    int i = 0;
    int x = 0;
    int y = 0;
    int s_count = 0;
    
    if(s_len == 0)
    {
        return s;
    }
    
    if(numRows == 1)
    {
        return s;
    }
    
    row_size = ceil((float)s_len / (2 * numRows - 2)) * (numRows - 1);
    col_size = numRows;
    
    array = malloc(sizeof(char) * row_size * col_size);
    new_s = malloc(sizeof(char) * (s_len + 1));
    memset(array, 0, sizeof(char) * row_size * col_size);
    memset(new_s, 0, sizeof(char) * (s_len + 1));
    
    for(i = 0; i < ceil((float)s_len / (2 * numRows - 2)); i++, offset += (numRows-1))
    {
        x = y = 0;
        while(y < numRows)
        {
            strncpy(&array[(y*row_size) + offset], &s[s_count], sizeof(char));
            s_count++;
            if(s_count == s_len)
            {
                goto out_of_s;
            }
            y++;
        }
        y--;
        while(x < numRows - 2)
        {
            y--;
            x++;
            strncpy(&array[(y*row_size) + offset + x], &s[s_count], sizeof(char));
            s_count++;
            if(s_count == s_len)
            {
                goto out_of_s;
            }
            
        }
    }
    
out_of_s:
    s_count = 0;
    i = 0;
    while(s_count < s_len)
    {
        if(array[i] != 0)
        {
            strncpy(&new_s[s_count], &array[i], sizeof(char));
            s_count++;
        }
        i++;
    }
    new_s[s_count] = '\0';
    
    return new_s;
}
