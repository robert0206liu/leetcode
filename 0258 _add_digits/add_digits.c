int addDigits(int num) {

    // https://en.wikipedia.org/wiki/Digital_root
    return num - 9 * ( (num - 1) / 9 );
}
