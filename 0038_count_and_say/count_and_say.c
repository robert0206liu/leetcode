#define STR_SIZE 10240

char* countAndSay(int n) {
    int i = 0;
    char *s = malloc(sizeof(char)*STR_SIZE);
    char *buf = malloc(sizeof(char)*STR_SIZE);
    char current_s[1] = {0};
    int s_len = 0;
    int s_index = 0;
    int buf_index = 0;
    int same = 0;
    memset(s, 0, sizeof(char)*STR_SIZE);
    memset(buf, 0, sizeof(char)*STR_SIZE);
    strncpy(s, "1\0", 2);
    
    if ((n == 0) || (n == 1))
    {
        return s;
    }
    
    for (i = 0; i<n-1; i++)
    {
        same = 0;
        buf_index = 0;
        s_len = strlen(s);
        strncpy(current_s, &s[0], 1);
        for (s_index = 0; s_index <= s_len; s_index++)
        {
            if(strncmp(current_s, &s[s_index], 1) == 0)
            {
                same++;
            }
            else
            {
                // How many
                sprintf(&buf[buf_index], "%d", same);
                buf_index++;
                // Which number
                strncpy(&buf[buf_index], current_s, 1);
                buf_index++;
                
                strncpy(current_s, &s[s_index], 1);
                same = 1;
            }
        }
        strncat( buf, "\0", 1);
        strncpy( s, buf, strlen(buf));
        memset(buf, 0, sizeof(char)*STR_SIZE);
    }
    
    return s;
}
