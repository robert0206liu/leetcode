char* countAndSay(int n) {
    if ((n == 0) || (n == 1))
    {
        return "1";
    }
    
    int i = 0;
    char *s = calloc(2, sizeof(char));
    s[0] = '1';
    s[1] = '\0';
    char *buf = NULL;
    char *buf_addr = NULL;  // for free()
    int s_index = 0;
    int target_num_index = 0;
    int s_len = 1;
    int next_s_len = 0;
    int same = 0;
    
    for (i = 1; i<n; i++)
    {
        buf = calloc(s_len * 2 + 1, sizeof(char));
        buf_addr = buf;
        for (s_index = 0, target_num_index = 0, same = 1; s_index <= s_len; s_index++)
        {
            if(s[s_index+1] == s[target_num_index])
            {
                same++;
            }
            else
            {
                *buf = ('0' + same);
                *buf++;
                *buf = s[target_num_index];
                *buf++;
                next_s_len += 2;
                target_num_index = s_index+1;
                same = 1;
            }
        }
        *buf = '\0';
        s_len = next_s_len;
        next_s_len = 0;
        free(s);
        s = buf_addr;
    }
    
    return s;
}
