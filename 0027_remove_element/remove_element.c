int removeElement(int* nums, int numsSize, int val) {
    int i = 0;

    for(i=0; i<numsSize; i++)
    {
        if(nums[i] == val)
        {
            while(nums[numsSize-1] == val)
            {
                numsSize--;
                if((numsSize == 0) || (numsSize-1 < i))
                {
                    goto out;
                }
            }
            nums[i] = nums[numsSize-1];
            numsSize--;
        }
    }
out:
    return numsSize;
}
